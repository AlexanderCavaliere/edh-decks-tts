# edh-decks

## Description
A collection of json blobs for use with the tabletop simulator edh/oathbreaker mod
Put these in your `Saved Objects` directory

## Directory Locations

| Operating System | Directory Path |
| ---------------- | -------------- |
| Windows | `C:\Users\<your-user-name>\Documents\My Games\Tabletop Simulator\Saves\Saved Objects` |
| Linux Based | `/home/<your-user-name>/.local/share/Tabletop\ Simulator/Saves/Saved\ Objects` |

Put the json file you would like to use in one of the above directories

## Decks

Each deck is organized by color and has been given an arbitrary name by either the user or the [frogtown](https://www.frogtown.me) file converter.
